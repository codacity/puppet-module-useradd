# Define: useradd::key
define useradd::key (
  $user,
  $key,
) {
  if is_hash($key) {
    # Set key type
    $key_type = $key['type'] ? {
      undef => 'ssh-rsa',
      default => $key['type'],
    }
    
    # Set key name
    $key_name = $key['name'] ? {
      undef => "$user@puppet",
      default => $key['name'],
    }
    
    # If key is set then create the key
    if has_key($key, 'key') {
      ssh_authorized_key { "${user}_${key_type}_${key['key']}_${$key_name}":
        ensure => present,
        user => $user,
        type => $key_type,
        key => $key['key'],
        name => $key_name,
        require => User[$user],
      }
    }
  }
}
